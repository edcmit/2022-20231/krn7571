library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_textio.all;

library src_lib;
use src_lib.edc_common.all;

--
library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;



entity top_estimator_tb is
  generic (runner_cfg : string);
end;



architecture bench of top_estimator_tb is


  -- Clock period
  constant clk_period : time := 5 ns;
  -- Generics

  -- Ports
  signal clk : std_logic;
  signal reset : std_logic;
  signal data_from_file : std_logic_vector (19 downto 0);
  signal data_from_file_valid : std_logic;
  signal data_channel_output : complex10;
  signal data_channel_output_valid : std_logic;
  signal start_process : std_logic;
  

  signal running : boolean := true;
begin

  top_estimator_inst : entity src_lib.top_estimator
    port map (
      clk => clk,
      reset => reset,
      data_from_file => data_from_file,
      start_process => start_process,
      data_from_file_valid => data_from_file_valid,
      data_channel_output => data_channel_output,
      data_channel_output_valid => data_channel_output_valid
    );

    
  main : process

    variable im_csv : integer_array_t;
    variable re_csv : integer_array_t;
    variable line_im_csv, line_re_csv : integer;


  begin
    test_runner_setup(runner, runner_cfg);
    while test_suite loop
      if run("CSV_Import_Test") then
        info("CSV_Import_Test");

        im_csv := load_csv("matlab-files/OFDM_QPSK_10_SYMB_im.csv", 32, true);
        re_csv := load_csv("matlab-files/OFDM_QPSK_10_SYMB_re.csv", 32, true);
        -- initialize fsm
        reset <= '1';
        start_process <= '0';
        data_from_file <= (others => '0');
        data_from_file_valid <= '0';
        wait for clk_period;
        reset <= '0';
        start_process <= '1';
        wait for clk_period;
        start_process <= '0';
        for i in 0 to im_csv.height - 1 loop
            line_im_csv := get(im_csv, i);
            line_re_csv := get(re_csv, i);
            data_from_file <= std_logic_vector(to_signed(line_re_csv, 10)) & std_logic_vector(to_signed(line_im_csv, 10));
            data_from_file_valid <= '1';
            wait for clk_period;
        end loop;

        data_from_file <= (others => '0');
        data_from_file_valid <= '0';
        wait for 4000 * clk_period;
        
      end if;
    end loop;
        running <= false;
        wait for clk_period;
        test_runner_cleanup(runner);

  end process main;



  clk_process : process
  begin
  clk <= '1';
  wait for clk_period/2;
  clk <= '0';
  wait for clk_period/2;
  end process clk_process;





  printer: process
    variable outputs_re : integer_array_t;
    variable outputs_im : integer_array_t;
    begin

      outputs_re := new_1d;
      outputs_im := new_1d;

    -- While the simulation is running, append output data to our output vector
    while (running) loop
      wait until falling_edge(clk);
        if data_channel_output_valid = '1' then
          append(outputs_re, to_integer(data_channel_output.re));
          append(outputs_im, to_integer(data_channel_output.im));
        end if;
    end loop;

    -- When no more clock cycles are expected, write the file and free the
    -- memory used for the output vector
    save_csv(outputs_re,"channel_output_re.csv");
    save_csv(outputs_im,"channel_output_im.csv");
    deallocate(outputs_re);
    deallocate(outputs_im);
    wait;
  end process;




end;
