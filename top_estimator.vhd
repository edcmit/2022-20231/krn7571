library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library src_lib;
use src_lib.edc_common.all;

entity top_estimator is
    port (
        clk   : in std_logic;
        reset : in std_logic;
        start_process : in std_logic;
        data_from_file : in  std_logic_vector (19 downto 0);
        data_from_file_valid : in std_logic;
        
        data_channel_output : out complex10;
        data_channel_output_valid : out std_logic
        
    );
end entity;

architecture top_estimator_architecture of top_estimator is

    component fsm is
        port (
            clk   : in std_logic;
            reset : in std_logic;
            
            -- connection to prbs module:
            prbs_enable : out std_ulogic;
            prbs_signo   : in std_ulogic;
            prbs_reset : out std_ulogic;
    
            -- connection to interpolator
            hinf: out complex10;
            hsup: out complex10;
            valid: out std_ulogic;
            estim_valid: in std_ulogic;
            interpolator_reset : out std_ulogic;
    
            -- connection to dpmemory
            data: in std_logic_vector(19 downto 0);
            addr: out unsigned(10 downto 0);
            write_enable: out std_logic;
    
            start_estimation : in std_ulogic;
            fsm_finished : out std_ulogic
            
        );
    end component;

    component prbs is
        port (
            clk   : in std_ulogic;
            reset : in std_ulogic;
            enable: in std_ulogic;
            output: out std_ulogic 
            
        );
    end component;

    component bram is
        generic (
            DATA_WIDTH : integer := 20;  -- Width of the data bus
            ADDR_WIDTH : integer := 11   -- Width of the address bus
            );
          port (clk   : in  std_logic;                                 -- Clock signal, active on rising edge
                addri : in  unsigned (ADDR_WIDTH-1 downto 0);          -- Address
                datai : in  std_logic_vector (DATA_WIDTH-1 downto 0);  -- Input data
                we    : in  std_logic;                                 -- Write enable
                datao : out std_logic_vector (DATA_WIDTH-1 downto 0)   -- Output data
                );
    end component;

    component interpolator is
        port (
            clk : in std_logic;          -- Input clock, active on rising edge
            rst : in std_logic;          -- Asynchronous reset, active high
            inf : in complex10;          -- Low input
            sup : in complex10;          -- High input
            valid : in std_logic;        -- When ``'1'``, signals the interpolator to begin interpolating between ``inf`` and ``sup``
            estim : out complex10;       -- Interpolated output. Has a 12/16 scale factor, since it estimates 12 times the channel and discards 4 LSB
            estim_valid : out std_logic  -- Takes the value ``'1'`` when ``estim`` is valid
            
        );
    end component;

    type estimator_state is (idle, fill_memory, run_fsm, estimation_finished);

    signal est_state_curr, est_state_next : estimator_state;
    signal write_enable_ram : std_logic;
    signal data_in_ram, data_out_ram : std_logic_vector (19 downto 0);
    

    -- Equalizer signals
    signal activate_equalizer : std_logic := '0';
    

    -- RAM signals
    signal address_counter, address_counter_next : unsigned(10 downto 0);
    signal addr_ram : unsigned(10 downto 0);

    -- FSM signals
    signal start_estimation_fsm : std_logic := '0';
    signal fsm_finished : std_logic := '0';
    signal reset_fsm : std_logic;
    signal prbs_enable, prbs_signo, prbs_reset : std_logic;
    signal hinf, hsup : complex10;
    signal valid, estim_valid : std_logic;
    -- signal data : std_logic_vector(19 downto 0);
    signal addr_out_fsm : unsigned(10 downto 0);
    signal write_enable : std_logic;

    -- Interpolator signals
    signal rst_interpolator : std_logic;
    signal estim_out_interpolator : complex10;
    

begin 

    -- port maps

    mybram : bram
    port map (
        clk => clk,
        addri => addr_ram,
        datai => data_in_ram,
        we => write_enable_ram,
        datao => data_out_ram
    );

    fsm_inst : fsm
    port map (
        clk => clk,
        reset => reset_fsm,
        prbs_enable => prbs_enable,
        prbs_signo => prbs_signo,
        prbs_reset => prbs_reset,
        hinf => hinf,
        hsup => hsup,
        valid => valid,
        estim_valid => estim_valid,
        data => data_out_ram,
        addr => addr_out_fsm,
        write_enable => write_enable,
        start_estimation => start_estimation_fsm,
        interpolator_reset => rst_interpolator,
        fsm_finished => fsm_finished
    );

    interpolator_inst : interpolator
        port map (
            clk => clk,
            rst => rst_interpolator,
            inf => hinf,
            sup => hsup,
            valid => valid,
            estim => estim_out_interpolator,
            estim_valid => estim_valid
        );

    prbs_inst : prbs
        port map (
          clk => clk,
          reset => prbs_reset,
          enable => prbs_enable,
          output => prbs_signo
        );
      



    -- processes

    main : process (all)
    begin
        case est_state_curr is
            when idle =>
                write_enable_ram <= '0';
                reset_fsm <= '1';
                address_counter_next <= (others => '0');
                start_estimation_fsm <= '0';

                if start_process = '1' then
                    est_state_next <= fill_memory;
                else
                    est_state_next <= idle;
                end if;

            when fill_memory =>
                write_enable_ram <= '1';
                addr_ram <= address_counter;
                data_in_ram <= data_from_file;
                address_counter_next <= address_counter + 1;

                if address_counter = 1705 then
                    est_state_next <= run_fsm;
                else
                    est_state_next <= fill_memory;    
                end if;

            when run_fsm =>
                reset_fsm <= '0';
                start_estimation_fsm <= '1';
                write_enable_ram <= '0';
                activate_equalizer <= '1';
                addr_ram <= addr_out_fsm;

                if fsm_finished = '0' then
                    est_state_next <= run_fsm;
                else
                    est_state_next <= estimation_finished;
                    
                end if;

                if estim_valid = '1' then
                    data_channel_output_valid <= '1';
                    data_channel_output <= estim_out_interpolator;
                else 
                    data_channel_output_valid <= '0';
                end if;

            when estimation_finished =>

                est_state_next <= idle;

        end case;
    end process;



    sync : process(clk, reset)
    begin
        if reset = '1' then
            est_state_curr <= idle;
        elsif rising_edge(clk) then
            est_state_curr <= est_state_next;
            address_counter <= address_counter_next;
        end if;
    end process;


end architecture;


