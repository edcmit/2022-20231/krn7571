library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;
use src_lib.edc_common.all;
--
library vunit_lib;
context vunit_lib.vunit_context;

entity fsm_tb is
  generic (runner_cfg : string);
end;

architecture bench of fsm_tb is


  -- Clock period
  constant clk_period : time := 5 ns;
  -- Generics

  -- Ports
  signal clk : std_logic;
  signal reset : std_logic;
  signal prbs_enable : std_ulogic;
  signal prbs_signo : std_ulogic;
  signal hinf : complex10;
  signal hsup : complex10;
  signal valid : std_ulogic;
  signal estim_valid : std_ulogic;
  signal data : std_logic_vector(19 downto 0);
  signal addr : std_logic_vector(10 downto 0);
  signal write_enable : std_logic;
  signal start_estimation : std_ulogic;

begin

  fsm_inst : entity src_lib.fsm
    port map (
      clk => clk,
      reset => reset,
      prbs_enable => prbs_enable,
      prbs_signo => prbs_signo,
      hinf => hinf,
      hsup => hsup,
      valid => valid,
      estim_valid => estim_valid,
      data => data,
      addr => addr,
      write_enable => write_enable,
      start_estimation => start_estimation
    );

  main : process
  begin
    test_runner_setup(runner, runner_cfg);
    while test_suite loop
      if run ("Run_Usecase") then
        info("Usecase run alive");
        reset <= '1';
        wait for 5 * clk_period;
        reset <= '0';
        prbs_signo <= '1';
        data <= "10011101011001010110";
        start_estimation <= '1';
        wait for 5 * clk_period;
        estim_valid <= '1';
        wait for 1500 * clk_period;
        start_estimation <= '1';
        wait for 1500 * clk_period;
      end if;
    end loop;
      test_runner_cleanup(runner);
  end process main;

  
  
  clk_process : process
  begin
    clk <= '1';
    wait for clk_period/2;
    clk <= '0';
    wait for clk_period/2;
  end process clk_process;

end;
