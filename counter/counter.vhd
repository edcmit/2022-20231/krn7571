library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity counter is

    generic (
        Bits: integer := 11;            -- number of bits to represent all adresses
        max_count: integer := 1705      -- highest needed adress
    );

    port (
        clk   : in std_logic;
        reset : in std_logic;
        enable: in std_logic;
        adress_out: out std_logic_vector(Bits - 1 downto 0)
    );
end entity;

architecture architecture_counter of counter is

    signal cont, p_cont: unsigned(Bits-1 downto 0);

begin

    comb: process(enable, cont)
    begin
        if enable = '1' then
            p_cont <= cont + 1;
        else
            p_cont <= cont;
        end if;
    end process;

    sync: process(reset, clk)
    begin
        if reset = '1' then
            cont <= (others => '0');
        elsif rising_edge(clk) then
            cont <= p_cont;
        end if;
    end process;

    adress_out <= std_logic_vector(cont);

end architecture;

