library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library src_lib;
use src_lib.edc_common.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library osvvm;
use osvvm.randompkg.all;
use osvvm.coveragepkg.all;
context osvvm.OsvvmContext;


package transactions_estimator is

    type input_tran_t is record
        reset   : boolean;
        cycles  : integer;
        data_from_file : std_logic_vector(19 downto 0);
        data_from_file_valid : boolean;
        
      end record;
    
      type output_tran_t is record
        data_channel_output_re : signed(9 downto 0);
        data_channel_output_im : signed(9 downto 0);
        data_channel_output_valid : boolean;
      end record;

        -- Message receivers, one for each entity that will receive transactions
        -- The checker has two receivers, so it can easily differentiate between
        -- actual and expected transactions
        constant driver_receiver : actor_t := new_actor("driver receiver");
        constant predictor_receiver : actor_t := new_actor("predictor receiver");
        constant checker_receiver_actual : actor_t := new_actor("checker receiver actual");
        constant checker_receiver_expected : actor_t := new_actor("checker receiver expected");

        -- For functional coverage:
        constant coverage_receiver_input : actor_t := new_actor("coverage receiver input");
        constant coverage_receiver_output : actor_t := new_actor("coverage receiver output");

        -- Objects for functional coverage
        signal cover_reset: CoverageIDtype := NewID("cover_reset");
        signal cover_enable: CoverageIDtype := NewID("cover_ena");
        signal cover_cycles: CoverageIDtype := NewID("cover_cycles");
        signal cover_value: CoverageIDtype := NewID("cover_value");

        -- Predefinition of functions and procedures in body
        procedure push(msg : msg_t; input_tran : input_tran_t);
        impure function pop(msg : msg_t) return input_tran_t;
        function to_string(input_tran : input_tran_t) return string;     
        procedure push(msg : msg_t; output_tran : output_tran_t);
        impure function pop(msg : msg_t) return output_tran_t;
        function to_string(output_tran : output_tran_t) return string;

    
end package;



package body transactions_estimator is

     -- Since VUnit's communication library only allows to push and pop simple
  -- datatypes, we'll define our own procedures to push and pop complete
  -- transactions, using the already-defined push and pop procedures
  -- We will also define our own to_string() functions for debugging purposes
  procedure push(msg: msg_t; input_tran: input_tran_t) is
  begin
    push(msg, input_tran.reset);
    push(msg, input_tran.cycles);
    push(msg, input_tran.data_from_file);
    push(msg, input_tran.data_from_file_valid);
    
  end procedure;

  -- Impure functions can read and write out of their scope, they have
  -- 'side effects'. Our 'pop' must be impure since it calls VUnit's 'pop'
  -- functions, which are impure themselves
  impure function pop(msg: msg_t) return input_tran_t is
    variable input_tran : input_tran_t;
  begin
    input_tran.reset  := pop(msg);
    input_tran.cycles := pop(msg);
    input_tran.data_from_file := pop(msg);
    input_tran.data_from_file_valid := pop(msg);
    return input_tran;
  end function;

  function to_string(input_tran: input_tran_t) return string is
  begin
    return "[input_tran] (reset => " & to_string(input_tran.reset) &  ", cycles => " & to_string(input_tran.cycles) & ")"; 
  end function;

  procedure push(msg: msg_t; output_tran: output_tran_t) is
  begin
    push(msg, output_tran.data_channel_output_re);
    push(msg, output_tran.data_channel_output_im);
    push(msg, output_tran.data_channel_output_valid);
  end procedure;

  impure function pop(msg: msg_t) return output_tran_t is
    variable output_tran : output_tran_t;
  begin
    output_tran.data_channel_output_re := pop(msg);
    output_tran.data_channel_output_im := pop(msg);
    output_tran.data_channel_output_valid := pop(msg);
    return output_tran;
  end function;

  function to_string(output_tran: output_tran_t) return string is
  begin
    return "[output_tran] (value => " &  ")";
  end function;

    
end package body;