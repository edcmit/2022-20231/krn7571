library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library src_lib;
use src_lib.edc_common.all;

entity equalizer is
    port (
        clk   : in std_logic;
        reset : in std_logic;
        activate_equalizer : in std_logic;

        estimated_data : in complex10;
        estimated_data_valid : in std_logic;

        y : in complex10;
        y_valid : in std_logic;

        equalized_data_re : out signed(19 downto 0);
        equalized_data_im : out signed(19 downto 0);
        equalized_data_valid : out std_logic
    );
end entity;




architecture architecture_equalizer of equalizer is

    signal re_sqared : signed(19 downto 0);
    signal im_sqared : signed(19 downto 0);

    signal ac, bd, ad, bc : signed(19 downto 0);
    signal result_re, result_im : signed(19 downto 0);

    type equalizer_state is (idle, read_inputs, calculation, setup_div1, setup_div2, wait_div1, wait_div2, complex_multiply1, complex_multiply2);
    signal eq_state_curr, eq_state_next : equalizer_state;

    signal channel_data_re, channel_data_im : signed(9 downto 0);
    signal channel_data_im_conj : signed(9 downto 0);
    signal div_result_re, div_result_im : signed(9 downto 0);

    -- divider1 and divider2
    signal N : integer := 20;
    signal M : integer := 20;
    
    signal dividend1, dividend2 : signed(N-1 downto 0);
    signal divisor1, divisor2 : signed(M-1 downto 0);
    signal valid1 : std_logic;
    signal quotient1 : signed(N-1 downto 0);
    signal remainder1 : signed(M-1 downto 0);
    signal out_valid1 : std_logic;
    signal busy1 : std_logic;
    signal err1 : std_logic;
    signal rst1 : std_logic;
    

    component divider
        generic (
          N : integer;
          M : integer
        );
          port (
          dividend : in signed (N-1 downto 0);
          divisor : in signed (M-1 downto 0);
          valid : in std_logic;
          quotient : out signed (N-1 downto 0);
          remainder : out signed (M-1 downto 0);
          out_valid : out std_logic;
          busy : out std_logic;
          err : out std_logic;
          rst : in std_logic;
          clk : in std_logic
        );
      end component;
      

begin

    divider_inst1 : divider
        generic map (
            N => N,
            M => M
        )
        port map (
            dividend => dividend1,
            divisor => divisor1,
            valid => valid1,
            quotient => quotient1,
            remainder => remainder1,
            out_valid => out_valid1,
            busy => busy1,
            err => err1,
            rst => rst1,
            clk => clk
        );


    main : process (all)
    begin 

        case eq_state_curr is
            when idle =>

                rst1 <= '1';
                equalized_data_valid <= '0';
                dividend1 <= (others => '0');
                dividend2 <= (others => '0');

                
                if activate_equalizer then
                    eq_state_next <= read_inputs;
                else 
                    eq_state_next <= idle;
                end if;
            
            when read_inputs =>
                if estimated_data_valid then
                    channel_data_re <= estimated_data.re;
                    channel_data_im <= estimated_data.im;
                    
                    eq_state_next <= calculation;
                else 
                    eq_state_next <= read_inputs;
                end if;
                
            when calculation => 
                
                re_sqared <= channel_data_re * channel_data_re;
                im_sqared <= channel_data_im * channel_data_im;

                eq_state_next <= setup_div1;

            when setup_div1 =>
                rst1 <= '0';
                
                dividend1(19 downto 10) <= channel_data_re;
                divisor1 <= re_sqared + im_sqared;
                dividend2(19 downto 10) <= channel_data_im;
                divisor2 <= re_sqared + im_sqared;
                valid1 <= '1';

                eq_state_next <= wait_div1;                
            
            when wait_div1 => 
                valid1 <= '0';
                
                if (out_valid1) then
                    
                    div_result_re <= quotient1(9 downto 0);
                    eq_state_next <= setup_div2;
                else
                    eq_state_next <= wait_div1;
                end if;

            when setup_div2 =>

                if busy1 = '0' then
                    dividend1 <= dividend2;
                    divisor1 <= divisor2;
                    valid1 <= '1';
                    eq_state_next <= wait_div2;
                else
                    eq_state_next <= setup_div2;
                end if;
            
            when wait_div2 =>

                valid1 <= '0';
                if out_valid1 then
                    div_result_im <= quotient1(9 downto 0);
                    eq_state_next <= complex_multiply1;
                else
                    eq_state_next <= wait_div2;
                end if;

            when complex_multiply1 =>
                
                if y_valid then
                    ac <= div_result_re * y.re;
                    bd <= div_result_im * y.im;
                    ad <= div_result_re * y.im;
                    bc <= div_result_im * y.re;

                    eq_state_next <= complex_multiply2;
                else 
                    eq_state_next <= complex_multiply1;
                end if;
                
            when complex_multiply2 =>
                
                result_re <= ac + bd; 
                result_im <= bc - ad;
                equalized_data_valid <= '1';
                equalized_data_re <= result_re(19 downto 0);
                equalized_data_im <= result_im(19 downto 0);
                eq_state_next <= idle;


            when others =>
                eq_state_next <= idle;
        end case;

    end process; 



    sync : process(reset, clk)
    begin
        if reset = '1' then
            eq_state_curr <= idle;
        elsif rising_edge(clk) then
            eq_state_curr <= eq_state_next;
            
            
        end if;
        
    end process;

end architecture;

