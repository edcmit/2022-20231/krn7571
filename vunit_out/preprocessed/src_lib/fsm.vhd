library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;
use src_lib.edc_common.all;

entity fsm is

    port (
        clk   : in std_logic;
        reset : in std_logic;
        
        -- connection to prbs module:
        prbs_enable : out std_ulogic;
        prbs_signo   : in std_ulogic;
        prbs_reset : out std_ulogic;

        -- connection to interpolator
        hinf: out complex10;
        hsup: out complex10;
        valid: out std_ulogic;
        estim_valid: in std_ulogic;
        interpolator_reset : out std_ulogic;

        -- connection to dpmemory
        data: in std_logic_vector(19 downto 0);
        addr: out unsigned(10 downto 0);
        write_enable: out std_logic;

        start_estimation : in std_ulogic;
        fsm_finished : out std_ulogic

    );
end entity;

architecture architecture_fsm of fsm is

    type state_type is (wait_interp, inf_pilot, sup_pilot, act_interp, act_prbs, rest);
    signal state, p_state : state_type; 
    signal counter : unsigned(3 downto 0);
    signal p_counter : unsigned(3 downto 0);
    signal addr_low, p_addr_low : unsigned(10 downto 0);
    signal addr_high, p_addr_high : unsigned(10 downto 0);
    signal buffer_low : std_logic_vector(19 downto 0);
    signal buffer_high : std_logic_vector(19 downto 0);

    signal sign_high, sign_low : std_ulogic;
    
    -- signal next_pilot: std_logic;
    
begin

    sync: process (reset, clk)
    begin
        if reset = '1' then
            state <= rest;
        elsif rising_edge(clk) then
            state <= p_state;
            counter <= p_counter;
            addr_high <= p_addr_high;
            addr_low <= p_addr_low;
        end if;
    end process;


    state_manage: process (all)
    begin
        
        -- p_state <= state;

        case state is
            when rest =>
                p_counter <= to_unsigned(1, 4);
                prbs_enable <= '0';
                prbs_reset <= '1';
                p_addr_low <= (others => '0');
                p_addr_high <= "00000001100";
                valid <= '0';
                fsm_finished <= '0';
                interpolator_reset <= '1';
                if start_estimation = '1' then
                    p_state <= inf_pilot;
                else 
                    p_state <= rest;
                end if;

        
            when inf_pilot =>
                
                addr <= addr_low;
                buffer_low <= data;
                p_state <= sup_pilot;
                prbs_reset <= '0';
                prbs_enable <= '1';
                sign_low <= prbs_signo;

            
            when sup_pilot =>
                if counter = 0 then
                    prbs_enable <= '1';
                    p_counter <= counter + 1;
                    
                
                elsif counter < 12 then
                    p_counter <= counter + 1;
                    addr <= addr_high;
                    

                elsif counter = 12 then
                    p_counter <= (others => '0');
                    p_state <= act_interp;
                    buffer_high <= data;
                    sign_high <= prbs_signo;
                    prbs_enable <= '0';
                end if;
            
            when act_interp => 
                
                if sign_low then
                    hinf.re <= -signed(buffer_low(19 downto 10));
                    hinf.im <= -signed(buffer_low(9 downto 0));
                else
                    hinf.re <= signed(buffer_low(19 downto 10));
                    hinf.im <= signed(buffer_low(9 downto 0));                    
                end if;

                if sign_high then
                    hsup.re <= -signed(buffer_high(19 downto 10));
                    hsup.im <= -signed(buffer_high(9 downto 0));
                else
                    hsup.re <= signed(buffer_high(19 downto 10));
                    hsup.im <= signed(buffer_high(9 downto 0));
                end if;
                
                interpolator_reset <= '0';
                valid <= '1';
                p_state <= wait_interp;
            
            when wait_interp =>
                valid <= '0';
                if estim_valid = '0' then
                    if addr_high = "11010101000" then
                        p_state <= rest;
                        fsm_finished <= '1';
                    
                    else
                        p_state <= act_prbs;
                    end if;
                else
                    p_state <= wait_interp;
                    fsm_finished <= '0';                        
                    
                end if;
        
            when act_prbs =>
            
                p_addr_high <= addr_high + 12;
                buffer_low <= buffer_high;
                sign_low <= sign_high;
                p_state <= sup_pilot;
                prbs_reset <= '0';
                

            when others => 
                p_state <= rest;
                
                
        end case;

    end process;
    

end architecture;