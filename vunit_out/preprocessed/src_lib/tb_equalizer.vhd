library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;
use src_lib.edc_common.all;
--
library vunit_lib;
context vunit_lib.vunit_context;

entity equalizer_tb is
  generic (runner_cfg : string);
end;

architecture bench of equalizer_tb is


  -- Clock period
  constant clk_period : time := 5 ns;
  -- Generics

  -- Ports equalizer
  signal clk : std_logic;
  signal reset_equalizer : std_logic;
  signal activate_equalizer : std_logic;
  signal estimated_data : complex10;
  signal estimated_data_valid : std_logic;
  signal y : complex10;
  signal y_valid : std_logic;
  signal equalized_data_re : signed(19 downto 0);
  signal equalized_data_im : signed(19 downto 0);
  signal equalized_data_valid : std_logic;

  -- Ports estimator
  signal reset_estimator : std_logic;
  signal data_from_file : std_logic_vector (19 downto 0);
  signal data_from_file_valid : std_logic;
  signal data_channel_output : complex10;
  signal data_channel_output_valid : std_logic;
  signal start_process : std_logic;  

  signal running : boolean := true;

  signal counter : unsigned(10 downto 0);
  signal counter_max : unsigned(10 downto 0);

begin

    equalizer_inst : entity src_lib.equalizer
        port map (
        clk => clk,
        reset => reset_equalizer,
        activate_equalizer => activate_equalizer,
        estimated_data => estimated_data,
        estimated_data_valid => estimated_data_valid,
        y => y,
        y_valid => y_valid,
        equalized_data_re => equalized_data_re,
        equalized_data_im => equalized_data_im,
        equalized_data_valid => equalized_data_valid
        );

    top_estimator_inst : entity src_lib.top_estimator
        port map (
        clk => clk,
        reset => reset_estimator,
        data_from_file => data_from_file,
        start_process => start_process,
        data_from_file_valid => data_from_file_valid,
        data_channel_output => data_channel_output,
        data_channel_output_valid => data_channel_output_valid
        );
        
  main : process

    variable im_csv : integer_array_t;
    variable re_csv : integer_array_t;
    variable line_im_csv, line_re_csv : integer;

    variable outputs_re : integer_array_t;
    variable outputs_im : integer_array_t;

    variable output_equalizer_re : integer_array_t;
    variable output_equalizer_im : integer_array_t;

    

  begin
    test_runner_setup(runner, runner_cfg);
    while test_suite loop
      if run("Equalizer Test") then
        
        -- Run estimator

        im_csv := load_csv("matlab-files/OFDM_QPSK_10_SYMB_im.csv", 32, true);
        re_csv := load_csv("matlab-files/OFDM_QPSK_10_SYMB_re.csv", 32, true);

        outputs_re := new_1d;
        outputs_im := new_1d;
        output_equalizer_re := new_1d;
        output_equalizer_im := new_1d;
        -- initialize fsm
        counter <= (others => '0');
        counter_max <= to_unsigned(1704, 11);
        reset_estimator <= '1';
        reset_equalizer <= '1';
        start_process <= '0';
        data_from_file <= (others => '0');
        data_from_file_valid <= '0';
        wait for clk_period;
        reset_estimator <= '0';
        start_process <= '1';
        wait for clk_period;
        start_process <= '0';
        for i in 0 to im_csv.height - 1 loop
            line_im_csv := get(im_csv, i);
            line_re_csv := get(re_csv, i);
            data_from_file <= std_logic_vector(to_signed(line_re_csv, 10)) & std_logic_vector(to_signed(line_im_csv, 10));
            data_from_file_valid <= '1';
            wait for clk_period;
        end loop;
        report("Data transfered");
        data_from_file <= (others => '0');
        data_from_file_valid <= '0';
        
        wait for clk_period;

        -- Collect Outputs 
        while counter < counter_max loop
            wait until falling_edge(clk);
              if data_channel_output_valid = '1' then
                append(outputs_re, to_integer(data_channel_output.re));
                append(outputs_im, to_integer(data_channel_output.im));
                counter <= counter + to_unsigned(1, 11);
                
              end if;
          end loop;
        
        wait for clk_period;
        report("All Outputs collected");
        -- Run Equalizer
        reset_equalizer <= '0';
        
        report("number of cycles to go: " & integer'image(outputs_re.length));
        for i in 0 to outputs_re.length - 1 loop  
            activate_equalizer <= '1';
            estimated_data.re <= to_signed(get(outputs_re, i), 10);
            estimated_data.im <= to_signed(get(outputs_im, i), 10);
            y.re <= to_signed(get(re_csv, i), 10);
            y.im <= to_signed(get(im_csv, i), 10);

            y_valid <= '1';
            estimated_data_valid <= '1';

            wait for clk_period;
            activate_equalizer <= '0';
            
            wait until falling_edge(equalized_data_valid);
            append(output_equalizer_re, to_integer(equalized_data_re));
            append(output_equalizer_im, to_integer(equalized_data_im));
        end loop; 
        
        save_csv(output_equalizer_re, "output_equalizer_re.csv");
        save_csv(output_equalizer_im, "output_equalizer_im.csv");
        deallocate(output_equalizer_im);
        deallocate(output_equalizer_re);
        save_csv(outputs_re,"channel_output_re.csv");
        save_csv(outputs_im,"channel_output_im.csv");
        deallocate(outputs_re);
        deallocate(outputs_im);

        elsif run("Isolation") then
          reset_equalizer <= '1';
          wait for clk_period;
          reset_equalizer <= '0';
          activate_equalizer <= '1';
          estimated_data.re <= to_signed(250, 10);
          estimated_data.im <= to_signed(100, 10);
          estimated_data_valid <= '1';
          y.re <= to_signed(125, 10);
          y.im <= to_signed(200, 10);
          y_valid <= '1';
          wait for clk_period;
          activate_equalizer <= '0';
          wait for 2000 * clk_period;

        end if;
    end loop;
        running <= false;
        wait for clk_period;
        test_runner_cleanup(runner);
  end process main;



  clk_process : process
  begin
  clk <= '1';
  wait for clk_period/2;
  clk <= '0';
  wait for clk_period/2;
  end process clk_process;

  
end;
