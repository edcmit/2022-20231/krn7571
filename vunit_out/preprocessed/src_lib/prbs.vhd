library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity prbs is
    port (
        clk   : in std_ulogic;
        reset : in std_ulogic;
        enable: in std_ulogic;
        output: out std_ulogic 

    );

end entity;

architecture architecture_prbs of prbs is

    signal p_psb, psb: std_ulogic_vector(10 downto 0);

begin

    output <= psb(10);                                  -- read output from pipeline

    comb: process(enable, psb)
        begin
            if (enable = '1') then
                p_psb(10 downto 1) <= psb(9 downto 0);  -- shift pipeline
                p_psb(0) <= psb(10) XOR psb(8);         -- calculate new incoming value
            else
                p_psb <= psb;
                
            end if;
        end process;


    sync: process(clk, reset)
        begin
        if (reset = '1') then
            psb <= (OTHERS => '1');                     -- reset full pipeline to ones
        elsif (rising_edge(clk)) then
            psb <= p_psb;
        end if;
        end process;
    

end architecture;
