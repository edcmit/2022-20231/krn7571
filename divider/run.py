from pathlib import Path
from vunit import VUnit
from subprocess import call
import sys

ROOT = Path(__file__).parent

# def post_run(results):
#     results.merge_coverage(file_name="coverage_data")
#     if vu.get_simulator_name() == "ghdl":
#         call(["gcovr", "coverage_data"])
#         call(["lcov","--capture", "--directory", ".", "--output", "coverage.info"])
#         call(["genhtml", "coverage.info", "--output-directory", "coverage_report"])

# Create VUnit instance by parsing command-line arguments
vu = VUnit.from_argv()
# Enable the communication library ('com')
vu.add_com()
# Enable OSVVM (Open Source VHDL Verification Methodology)
vu.add_osvvm()

# Define GHDL flags
a_flags = []    # Analyze flags
e_flags = []    # Elaborate flags
sim_flags = []  # Simulation options
sim_flags.append("--ieee-asserts=disable-at-0")  # Disable IEEE numeric-std warnings, but only at time 0

# Create library 'src_lib', where our files will be compiled
lib = vu.add_library("src_lib")

# Add all files ending in .vhd in current working directory to our library 'src_lib'
lib.add_source_files(ROOT / "*.vhd")

# Set options for reading VHDL files
lib.set_compile_option("ghdl.a_flags", a_flags)
# lib.set_compile_option("enable_coverage", True)
# Set options for elaboration
lib.set_sim_option("ghdl.elab_flags", e_flags)
# lib.set_sim_option("enable_coverage", True)
# Set compilation flags for the library
lib.set_sim_option("ghdl.sim_flags", sim_flags)

# Run vunit function
# vu.main(post_run=post_run)
vu.main()
