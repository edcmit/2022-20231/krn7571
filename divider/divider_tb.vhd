library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library osvvm;
use osvvm.randompkg.all;
use osvvm.coveragepkg.all;

entity divider_tb is
    generic (runner_cfg : string);
  end entity;
  
architecture rtl of divider_tb is

  constant CLK_PERIOD_c : time := 10 ns;

  constant DATA_SIZE_c : natural := 10;
  constant FRAC_SIZE_c : natural := 7;

  constant M_c : natural := DATA_SIZE_c;
  constant N_c : natural := DATA_SIZE_c + FRAC_SIZE_c;

  signal clk : std_logic;
  signal rst : std_logic;

  signal dividend   : signed(N_c-1 downto 0);
  signal divisor    : signed(M_c-1 downto 0);
  signal quotient   : signed(N_c-1 downto 0);
  signal remainder  : signed(M_c-1 downto 0);
  signal valid      : std_logic;
  signal out_valid  : std_logic;
  signal busy       : std_logic;
  signal err        : std_logic;
  
begin

  main : process
    
    function resize_dividend(
      D : integer;
      N : natural;
      F : natural
      ) return signed is
        variable result : signed(N+F-1 downto 0);
    begin
        result := to_signed(D, N) & to_signed(0, F);
      return result;
    end function;

    variable re_csv : integer_array_t;
    variable im_csv : integer_array_t;
    variable data_csv : integer_array_t;
    variable num_re_csv : integer;
    variable num_im_csv : integer;
    variable num_csv : integer;

  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
  
      if run("TEST_01") then
        info("Testing divider");

        dividend  <= resize_dividend(2, DATA_SIZE_c, FRAC_SIZE_c);
        divisor   <= to_signed(3, M_c);
        valid     <= '0';

        rst <= '1';
        wait for 2*CLK_PERIOD_c;
        rst <= '0';
        wait for CLK_PERIOD_c;
        valid <= '1';
        wait for CLK_PERIOD_c;
        valid <= '0';
        wait until out_valid='1';
        info("Result: 0x" & to_hex_string(std_logic_vector(quotient(DATA_SIZE_c-1 downto 0))) & "; 0b" & to_string(std_logic_vector(quotient(DATA_SIZE_c-1 downto 0))));
        wait for 5*CLK_PERIOD_c;

      elsif run("TEST_02") then
        info("Testing divider using CSV file");

        re_csv := load_csv("dividend.csv", 32, true);
        im_csv := load_csv("divisor.csv", 32, true);

        dividend  <= resize_dividend(0, DATA_SIZE_c, FRAC_SIZE_c);
        divisor   <= to_signed(0, M_c);
        valid     <= '0';

        rst <= '1';
        wait for 2*CLK_PERIOD_c;
        rst <= '0';
        wait for CLK_PERIOD_c;

        for i in 0 to re_csv.height-1 loop
          num_re_csv := get(re_csv, i);
          num_im_csv := get(im_csv, i);
          info("Real: " & to_string(num_re_csv) & "; Imag: " & to_string(num_im_csv));

          dividend <= resize_dividend(num_re_csv, DATA_SIZE_c, FRAC_SIZE_c);
          divisor <= to_signed(num_im_csv, DATA_SIZE_c);
          valid <= '1';
          wait for CLK_PERIOD_c;
          valid <= '0';
          wait until out_valid='1';
          info("Result: 0x" & to_hex_string(std_logic_vector(quotient(DATA_SIZE_c-1 downto 0))) & "; 0b" & to_string(std_logic_vector(quotient(DATA_SIZE_c-1 downto 0))));
          wait for CLK_PERIOD_c;
        end loop;

      end if;
  end loop;

    test_runner_cleanup(runner);  
  end process;
  
  
    clk_stim : process
  begin
    clk <= '0';
    wait for CLK_PERIOD_c/2;
    clk <= '1';
    wait for CLK_PERIOD_c/2;
  end process;
  
  uut : entity src_lib.divider
    generic map ( N => N_c,
                  M => M_c )
    port map ( 
      dividend => dividend,
      divisor => divisor,
      valid => valid,
      quotient => quotient,
      remainder => remainder,
      out_valid => out_valid,
      busy => busy,
      err => err,
      rst => rst,
      clk => clk
    );

end architecture;