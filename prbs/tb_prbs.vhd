library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

entity prbs_tb is
end;

architecture bench of prbs_tb is

  component prbs
      port (
      clk : in std_ulogic;
      reset : in std_ulogic;
      enable : in std_ulogic;
      output : out std_ulogic
    );
  end component;

  -- Clock period
  constant clk_period : time := 5 ns;
  signal endsim : boolean := false;
  -- Generics

  -- Ports
  signal clk : std_ulogic;
  signal reset : std_ulogic;
  signal enable : std_ulogic;
  signal output : std_ulogic;

  
  
begin

  prbs_inst : prbs
    port map (
      clk => clk,
      reset => reset,
      enable => enable,
      output => output
    );

  clk_process : process
  begin
    clk <= '1';
    wait for clk_period/2;
    clk <= '0';
    wait for clk_period/2;
    if endsim=true then
      wait;
    end if;
  end process clk_process;

  steering_process: process
  begin
    reset <= '1';
    enable <= '1';
    wait for 1 * clk_period;
    reset <= '0';
    wait for 100 * clk_period;
    endsim <= true;
    wait;
  end process;

  output_process: process(clk)
    variable row: line;
    file file_output: text open write_mode is "psb_sequence.csv";
  begin 
    if rising_edge(clk) then
      write(row, output);
      writeline(file_output, row);
    
    end if;
  end process;



end;
