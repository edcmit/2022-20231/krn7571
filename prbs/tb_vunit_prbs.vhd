library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;
--
library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

library osvvm;
use osvvm.randompkg.all;
use osvvm.coveragepkg.all;


entity prbs_vunit_tb is
  generic (runner_cfg : string);
end;

architecture bench_vunit of prbs_vunit_tb is


  -- Clock period
  constant clk_period : time := 5 ns;
  -- Generics

  -- Ports
  signal clk : std_ulogic;
  signal reset : std_ulogic;
  signal enable : std_ulogic;
  signal output : std_ulogic;

  type input_tran_t is record
    reset   : boolean;
    enable  : boolean;
    cycles  : integer;
  end record;

  type output_tran_t is record
    value : integer;  
  end record;

  signal running : boolean := true;

  -- Since VUnit's communication library only allows to push and pop simple
  -- datatypes, we'll define our own procedures to push and pop complete
  -- transactions, using the already-defined push and pop procedures
  -- We will also define our own to_string() functions for debugging purposes
  procedure push(msg: msg_t; input_tran: input_tran_t) is
  begin
    push(msg, input_tran.reset);
    push(msg, input_tran.enable);
    push(msg, input_tran.cycles);
  end procedure;

  -- Impure functions can read and write out of their scope, they have
  -- 'side effects'. Our 'pop' must be impure since it calls VUnit's 'pop'
  -- functions, which are impure themselves
  impure function pop(msg: msg_t) return input_tran_t is
    variable input_tran : input_tran_t;
  begin
    input_tran.reset  := pop(msg);
    input_tran.enable := pop(msg);
    input_tran.cycles := pop(msg);
    return input_tran;
  end function;

  function to_string(input_tran: input_tran_t) return string is
  begin
    return "[input_tran] (reset => " & to_string(input_tran.reset) & ", enable => " & to_string(input_tran.enable) & ", cycles => " & to_string(input_tran.cycles) & ")"; 
  end function;

  procedure push(msg: msg_t; output_tran: output_tran_t) is
  begin
    push(msg, output_tran.value);
  end procedure;

  impure function pop(msg: msg_t) return output_tran_t is
    variable output_tran : output_tran_t;
  begin
    output_tran.value := pop(msg);
    return output_tran;
  end function;

  function to_string(output_tran: output_tran_t) return string is
  begin
    return "[output_tran] (value => " & to_string(output_tran.value) & ")";
  end function;

  -- Message receivers, one for each entity that will receive transactions
  -- The checker has two receivers, so it can easily differentiate between
  -- actual and expected transactions
  constant driver_receiver : actor_t := new_actor("driver receiver");
  constant predictor_receiver : actor_t := new_actor("predictor receiver");
  constant checker_receiver_actual : actor_t := new_actor("checker receiver actual");
  constant checker_receiver_expected : actor_t := new_actor("checker receiver expected");

  -- For functional coverage:
  constant coverage_receiver_input : actor_t := new_actor("coverage receiver input");
  constant coverage_receiver_output : actor_t := new_actor("coverage receiver output");

  -- Objects for functional coverage
  signal cover_reset: CoverageIDtype := NewID("cover_reset");
  signal cover_enable: CoverageIDtype := NewID("cover_ena");
  signal cover_cycles: CoverageIDtype := NewID("cover_cycles");
  signal cover_value: CoverageIDtype := NewID("cover_value");


begin

  prbs_inst : entity src_lib.prbs
    port map (
      clk => clk,
      reset => reset,
      enable => enable,
      output => output
    );

  main : process

    procedure seq_tran(constant input_tran: in input_tran_t) is
      variable msg : msg_t;
      variable msg_copy : msg_t;
      variable msg_copy_cover: msg_t;
    begin
      info("test_sequence: pushing:" & to_string(input_tran));
      msg := new_msg;
      push(msg, input_tran);
      msg_copy := copy(msg);
      msg_copy_cover := copy(msg);
      send(net, driver_receiver, msg);
      send(net, predictor_receiver, msg_copy);
      send(net, coverage_receiver_input, msg_copy_cover);
      wait for input_tran.cycles*clk_period;
    end procedure;


  begin
    test_runner_setup(runner, runner_cfg);
    
    while test_suite loop

   
    if run("test_reset") then
      info("counter: test_reset");
      -- Test sequence for test_reset
      seq_tran((reset => true, enable => false, cycles => 1));
      seq_tran((reset => false, enable => true, cycles => 100));

    elsif run("test_overflow") then
      info("counter: test_overflow");

      seq_tran((reset => true, enable => false, cycles => 1));
      seq_tran((reset => false, enable => true, cycles => 258));

    elsif run("test_alternating_ena") then
      info("counter: test_alternating_enable");
     
      seq_tran((reset => true, enable => false, cycles => 1));

      for i in 0 to 200 loop
        seq_tran((reset => false, enable => true, cycles => 1));
        seq_tran((reset => false, enable => false, cycles => 1));
      end loop;
    
    end if;

  end loop;
    running <= false;
    wait for 10 ns;
    -- Print functional coverage
    writebin(cover_reset);
    writebin(cover_enable);
    writebin(cover_cycles);
    writebin(cover_value);

    test_runner_cleanup(runner);
  
  end process main;

  clk_process : process
  begin
  clk <= '1';
  wait for clk_period/2;
  clk <= '0';
  wait for clk_period/2;
  end process clk_process;

  driver: process
    variable msg: msg_t;
    variable input_tran: input_tran_t; 
  begin
    info("driver: waiting for transaction...");
    receive(net, driver_receiver, msg);
    input_tran := pop(msg);
    info("driver: received:" & to_string(input_tran));

    if input_tran.reset then
      reset <= '1';
    else
      reset <= '0';
    end if;

    if input_tran.enable then
      enable <= '1';
    else
      enable <= '0';
    end if;

    for i in 1 to input_tran.cycles loop
      wait for clk_period;
    end loop;
  end process;


  monitor: process
    variable msg: msg_t;
    variable output_tran: output_tran_t;
    variable msg_copy_cover: msg_t; 
    variable out_helper: std_logic_vector(0 downto 0);
  begin
    wait until falling_edge(clk);
    out_helper(0) := output; 
    output_tran.value := to_integer(unsigned(out_helper));
    info("monitor: detected:" & to_string(output_tran));
    msg := new_msg;
    push(msg, output_tran);
    msg_copy_cover := copy(msg);
    send(net, checker_receiver_actual, msg);
    send(net, coverage_receiver_output, msg_copy_cover);
  end process;

  printer: process
    variable outputs : integer_array_t;
    begin

      outputs := new_1d;

    -- While the simulation is running, append output data to our output vector
    while (running) loop
      wait until falling_edge(clk);
        if enable = '1' then
        append(outputs, to_integer(unsigned'('0' & output)));
        end if;
    end loop;

    -- When no more clock cycles are expected, write the file and free the
    -- memory used for the output vector
    save_csv(outputs,"prbs.csv");
    deallocate(outputs);
    wait;
  end process;



  -- predictor: process
  --   variable input_tran: input_tran_t;
  --   variable expected_output_tran: output_tran_t;
  --   variable expected_output: integer;
  --   variable msg: msg_t;
    
  --   variable p_psb, psb: std_logic_vector(10 downto 0);
  --   variable transform: std_logic_vector(0 downto 0);
  -- begin
  --   psb := (OTHERS => '1');
    
  --   while true loop
      
  --     receive(net, predictor_receiver, msg);
  --     input_tran := pop(msg);
  --     info("predictor: input:" & to_string(input_tran));

     

  --     for i in 0 to input_tran.cycles loop
  --       transform := psb(10);
  --       expected_output := to_integer(unsigned(transform));
  --       if input_tran.reset then
  --         expected_output := 1;
  --       elsif input_tran.enable then
  --         p_psb(10 downto 1) := psb(9 downto 0);
  --         p_psb(0) := psb(10) XOR psb(8);        
  --       end if;
  --       -- Create the output transaction and send it to the checker
  --       expected_output_tran.value := expected_output;
  --       info("predictor: expected:" & to_string(expected_output_tran));
  --       msg := new_msg;
  --       push(msg, expected_output_tran);
  --       send(net, checker_receiver_expected, msg);
  --     end loop;
  --   end loop;

  -- end process;

  -- checker: process
  --   variable msg_actual: msg_t;
  --   variable msg_expected: msg_t;
  --   variable output_tran_actual: output_tran_t;
  --   variable output_tran_expected: output_tran_t;
  -- begin
  --   -- Wait for both actual and expected output transactions
  --   receive(net, checker_receiver_actual, msg_actual);
  --   receive(net, checker_receiver_expected, msg_expected);
  --   -- Extract the transactions from the message
  --   output_tran_actual := pop(msg_actual);
  --   output_tran_expected := pop(msg_expected);
  --   info("checker: actual:" & to_string(output_tran_actual));
  --   info("checker: expected:" & to_string(output_tran_expected));
  --   -- Check transactions match
  --   check(output_tran_actual.value = output_tran_expected.value, "checker: actual value (" & to_string(output_tran_actual.value) & ") and expected value (" & to_string(output_tran_expected.value) & ") must be equal");
  -- end process;

  
  coverage_input: process
    variable msg: msg_t;
    variable input_tran: input_tran_t;

    function bool2int (bool: boolean) return integer is
      variable retval : integer;
    begin
      if bool then
        retval := 1;
      else
        retval := 0;
      end if;
      return retval;
    end;

  begin
    AddBins(cover_reset, GenBin(0,1));
    AddBins(cover_enable, GenBin(0,1));
    AddBins(cover_cycles, GenBin(0,511,4));
    while true loop
      receive(net, coverage_receiver_input, msg);
      input_tran := pop(msg);
      icover(cover_reset, bool2int(input_tran.reset));
      icover(cover_enable, bool2int(input_tran.enable));
      icover(cover_cycles, input_tran.cycles);
    end loop;
  end process;

  coverage_output: process
    variable msg: msg_t;
    variable output_tran: output_tran_t;
  begin
    AddBins(cover_value, GenBin(0,255,4));
    while true loop
      receive(net, coverage_receiver_output, msg);
      output_tran := pop(msg);
      icover(cover_value, output_tran.value);
    end loop;
  end process;


end;
