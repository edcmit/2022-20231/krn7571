from pathlib import Path
from vunit import VUnit

from subprocess import call

vu = VUnit.from_argv()

vu.add_com()
vu.add_osvvm()

lib = vu.add_library("src_lib")

lib.add_source_files("*.vhd")

lib.set_sim_option("enable_coverage", True)
lib.set_compile_option("enable_coverage", True)

def post_run(results):
    results.merge_coverage(file_name="coverage_data")
    if vu.get_simulator_name() == "ghdl":
        call(["gcovr", "coverage_data"])
        call(["lcov", '--capture', '--directory', '.', '--output', 'coverage.info'])
        call(["genhtml", "coverage.info", "--output-directory", "coverage_report"])

# Run vunit function, but pass to it2 the post_run hook
vu.main(post_run=post_run)
